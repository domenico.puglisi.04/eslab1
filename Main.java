public class Main {
  public static void main(String[] args) {
    Persona p1 = new Persona("Luca Alberto", "Di Marzo", 17);
    Persona p2 = new Persona("Domenico", "Puglisi", 16);
    
    Persona p3 = p2;
    
    System.out.println("Dettagli Persona 1:\t" + p1.dettagli());
    System.out.println("Dettagli Persona 2:\t" + p2.dettagli());
    System.out.println("Dettagli Persona 3:\t" + p3.dettagli());
    
    if(p2 == p3) {
      System.out.println("I riferimenti p2 e p3 puntano alla stessa area di area in memoria.");
    }
  }
}