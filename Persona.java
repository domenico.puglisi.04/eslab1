public class Persona {
  private String nome;
  private String cognome;
  private int eta;
  
  public Persona() {
    this.nome = "";
    this.cognome = "";
    this.eta = 0;
  }
  
  public Persona(String n, String c, int e) {
    this.nome = n;
    this.cognome = c;
    this.eta = e;
  }
  
  public Persona(Persona other) {
    this.nome = new String(other.nome);
    this.cognome = new String(other.cognome);
    this.eta = other.eta;
  }
  
  public String getNome()    { return nome;    }
  public String getCognome() { return cognome; }
  public int    getEta()     { return eta;     }
  
  public void setNome(String n) {
    this.nome = new String(n);
  }

  public void setCognome(String c) {
    this.cognome = new String(c);
  }

  public void setEta(int e) {
    this.eta = e;
  }
  
  public String dettagli() {
    return new String(nome + " " + cognome + " " + eta);
  }
}